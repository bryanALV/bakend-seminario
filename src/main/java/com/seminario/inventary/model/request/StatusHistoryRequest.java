package com.seminario.inventary.model.request;

import com.seminario.inventary.model.Product;

public class StatusHistoryRequest {

    private Long statusId;
    private Product product;
    private String desc;

    public StatusHistoryRequest() {
        //Constructor
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
