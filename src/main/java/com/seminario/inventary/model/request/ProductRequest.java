package com.seminario.inventary.model.request;


import java.time.Instant;

public class ProductRequest {

    private Long idPr;

    private String nombre;

    private Integer cant;

    private String pais;

    private Instant fecha;

    public ProductRequest() {
        //Constructor
    }

    public Long getIdPr() {
        return idPr;
    }

    public void setIdPr(Long idPr) {
        this.idPr = idPr;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCant() {
        return cant;
    }

    public void setCant(Integer cant) {
        this.cant = cant;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Instant getFecha() {
        return fecha;
    }

    public void setFecha(Instant fecha) {
        this.fecha = fecha;
    }
}
