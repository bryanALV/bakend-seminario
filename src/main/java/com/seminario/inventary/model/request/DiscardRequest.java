package com.seminario.inventary.model.request;

import com.seminario.inventary.model.Product;

public class DiscardRequest {

    private Long idDiscard;
    private Product product;
    private String reason;

    public DiscardRequest() {
        //constructor
    }

    public Long getIdDiscard() {
        return idDiscard;
    }

    public void setIdDiscard(Long idDiscard) {
        this.idDiscard = idDiscard;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
