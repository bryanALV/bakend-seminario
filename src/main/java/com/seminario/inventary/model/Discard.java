package com.seminario.inventary.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "despacho_producto")
public class Discard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "des_id")
    private Long idDiscard;

    @OneToOne
    @JoinColumn(name = "fk_producto", referencedColumnName = "pr_serial")
    private Product product;

    @Column(name = "des_motivo")
    private String reason;

    public Discard() {
        //Constructor
    }

    public Long getIdDiscard() {
        return idDiscard;
    }

    public void setIdDiscard(Long idDiscard) {
        this.idDiscard = idDiscard;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
