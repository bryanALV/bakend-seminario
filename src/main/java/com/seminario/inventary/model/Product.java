package com.seminario.inventary.model;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "producto")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pr_serial")
    private Long idPr;

    @Column(name = "pr_nombre")
    private String nombre;

    @Column(name = "pr_cant")
    private Integer cant;

    @Column(name = "pr_pais")
    private String pais;

    @Column(name = "pr_fecha")
    private Instant fecha;

    public Product() {
        //constructor
    }

    public Long getIdPr() {
        return idPr;
    }

    public void setIdPr(Long idPr) {
        this.idPr = idPr;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCant() {
        return cant;
    }

    public void setCant(Integer cant) {
        this.cant = cant;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Instant getFecha() {
        return fecha;
    }

    public void setFecha(Instant fecha) {
        this.fecha = fecha;
    }
}
