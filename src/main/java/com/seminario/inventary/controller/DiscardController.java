package com.seminario.inventary.controller;

import com.seminario.inventary.model.Discard;
import com.seminario.inventary.model.request.DiscardRequest;
import com.seminario.inventary.service.DiscardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/discard")
@Api(tags = "Discard")
public class DiscardController {

    private static final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private DiscardService discardService;

    @PostMapping(path = "/insert")
    @ApiOperation(value = "Insert Discard", response = Discard.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The Discard doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Discard insertDiscard(@RequestBody DiscardRequest discardRequest) {
        Discard discard = modelMapper.map(discardRequest, Discard.class);
        return discardService.insertDescard(discard);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Discard", response = Discard.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The Discard doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Discard updateDiscard(@RequestBody DiscardRequest discardRequest) {
        Discard discard = modelMapper.map(discardRequest, Discard.class);
        return discardService.updateDiscard(discard);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get All Discard", response = Discard.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The Discard doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<Discard> getAllDiscard(){
        return discardService.getAllDiscard();
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "Delete Discard", response = Discard.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The Discard doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public void deleteDiscard(@RequestParam(name = "id") Long id){
        discardService.deleteDiscard(id);
    }

    @GetMapping(path = "/product")
    @ApiOperation(value = "Get Discard By Product Id", response = Discard.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The Discard doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Discard getByProductId(@RequestParam(name = "id") Long productId){
        return discardService.getByProductId(productId);
    }
}
