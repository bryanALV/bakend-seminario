package com.seminario.inventary.controller;

import com.seminario.inventary.model.Product;
import com.seminario.inventary.model.request.ProductRequest;
import com.seminario.inventary.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/product")
@Api(tags = "Product")
public class ProductController {

    private static final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private ProductService productService;

    @PostMapping(path = "/insert")
    @ApiOperation(value = "Insert Product", response = Product.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The Product doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Product insertProduct(@RequestBody ProductRequest productRequest) {
        Product product = modelMapper.map(productRequest, Product.class);
        return productService.insertProducto(product);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Product", response = Product.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The Product doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Product updateProduct(@RequestBody ProductRequest productRequest) {
        Product product = modelMapper.map(productRequest, Product.class);
        return productService.updateProducto(product);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get All Product", response = Product.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The Product doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<Product> getAllProduct() {
        return productService.getAllProducto();
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "Delete Product", response = Product.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The Product doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public void deleteProduct(@RequestParam(name = "id") Long id) {
        productService.deleteProducto(id);
    }
}
