package com.seminario.inventary.controller;

import com.seminario.inventary.model.StatusHistory;
import com.seminario.inventary.model.request.StatusHistoryRequest;
import com.seminario.inventary.service.StatusHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/status/history")
@Api(tags = "Status History")
public class StatusHistoryController {

    private static final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private StatusHistoryService statusHistoryService;

    @PostMapping(path = "/insert")
    @ApiOperation(value = "Insert Status History", response = StatusHistory.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The Status History doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public StatusHistory insertStatusHistory(@RequestBody StatusHistoryRequest statusHistoryRequest) {
        StatusHistory statusHistory = modelMapper.map(statusHistoryRequest, StatusHistory.class);
        return statusHistoryService.insertStatusHistory(statusHistory);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Status History", response = StatusHistory.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The Status History doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public StatusHistory updateStatusHistory(@RequestBody StatusHistoryRequest statusHistoryRequest) {
        StatusHistory statusHistory = modelMapper.map(statusHistoryRequest, StatusHistory.class);
        return statusHistoryService.updateStatusHistory(statusHistory);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get All Status History", response = StatusHistory.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The Status History doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<StatusHistory> getAllStatusHistory() {
        return statusHistoryService.getAllStatusHistory();
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "Delete Status History", response = StatusHistory.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The Status History doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public void deleteStatusHistory(@RequestParam(name = "id") Long id) {
        statusHistoryService.deleteStatusHistory(id);
    }

    @GetMapping(path = "/product")
    @ApiOperation(value = "Get Status History By Product Id", response = StatusHistory.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The Status History doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<StatusHistory> getByProductId(@RequestParam(name = "id") Long productId) {
        return statusHistoryService.getByProductId(productId);
    }
}
