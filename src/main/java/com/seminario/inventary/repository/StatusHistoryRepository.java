package com.seminario.inventary.repository;

import com.seminario.inventary.model.StatusHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StatusHistoryRepository extends JpaRepository<StatusHistory, Long> {

    @Query(nativeQuery = true, value = "SELECT " +
            "id_estado, " +
            "fk_producto, " +
            "desc_es " +
            "FROM historico_estado_producto " +
            "WHERE fk_producto = :productId")
    List<StatusHistory> getByProductId(@Param(value = "productId") Long productId);
}
