package com.seminario.inventary.repository;

import com.seminario.inventary.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    Product getByIdPr(Long id);
}
