package com.seminario.inventary.repository;

import com.seminario.inventary.model.Discard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DiscardRepository extends JpaRepository<Discard, Long> {

    @Query(nativeQuery = true, value = "SELECT " +
            "des_id, " +
            "fk_producto, " +
            "des_motivo " +
            "FROM despacho_producto " +
            "WHERE fk_producto = :productId")
    Discard getByProductId(@Param(value = "productId") Long productId);
}
