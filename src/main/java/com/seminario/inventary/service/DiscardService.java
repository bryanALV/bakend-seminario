package com.seminario.inventary.service;

import com.seminario.inventary.model.Discard;
import com.seminario.inventary.model.Product;
import com.seminario.inventary.repository.DiscardRepository;
import com.seminario.inventary.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DiscardService {

    @Autowired
    private DiscardRepository discardRepository;

    @Autowired
    private ProductRepository productRepository;

    public Discard insertDescard (Discard discard){
        Product product = productRepository.getByIdPr(discard.getProduct().getIdPr());
        discard.setProduct(product);

        return discardRepository.save(discard);
    }

    public Discard updateDiscard (Discard discard){
        return discardRepository.save(discard);
    }

    public List<Discard> getAllDiscard (){
        return discardRepository.findAll();
    }

    public void deleteDiscard (Long id){
        discardRepository.deleteById(id);
    }

    public Discard getByProductId(Long productId){
        return discardRepository.getByProductId(productId);
    }
}
