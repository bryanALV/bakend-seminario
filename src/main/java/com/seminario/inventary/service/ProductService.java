package com.seminario.inventary.service;

import com.seminario.inventary.model.Product;
import com.seminario.inventary.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public Product insertProducto (Product product){
        return productRepository.save(product);
    }

    public Product updateProducto (Product product){
        return productRepository.save(product);
    }

    public List<Product> getAllProducto (){
        return productRepository.findAll();
    }

    public void deleteProducto (Long id){
        productRepository.deleteById(id);
    }
}
