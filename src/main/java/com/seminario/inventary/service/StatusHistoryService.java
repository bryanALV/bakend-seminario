package com.seminario.inventary.service;

import com.seminario.inventary.model.Product;
import com.seminario.inventary.model.StatusHistory;
import com.seminario.inventary.repository.ProductRepository;
import com.seminario.inventary.repository.StatusHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusHistoryService {

    @Autowired
    private StatusHistoryRepository statusHistoryRepository;

    @Autowired
    private ProductRepository productRepository;

    public StatusHistory insertStatusHistory(StatusHistory statusHistory) {
        Product product = productRepository.getByIdPr(statusHistory.getProduct().getIdPr());
        statusHistory.setProduct(product);

        return statusHistoryRepository.save(statusHistory);
    }

    public StatusHistory updateStatusHistory(StatusHistory statusHistory) {
        return statusHistoryRepository.save(statusHistory);
    }

    public List<StatusHistory> getAllStatusHistory() {
        return statusHistoryRepository.findAll();
    }

    public void deleteStatusHistory(Long id) {
        statusHistoryRepository.deleteById(id);
    }

    public List<StatusHistory> getByProductId(Long productId) {
        return statusHistoryRepository.getByProductId(productId);
    }
}
