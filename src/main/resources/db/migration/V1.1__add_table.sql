CREATE TABLE "producto" (
  pr_serial serial NOT NULL,
  pr_nombre CHARACTER VARYING (60) NOT NULL,
  pr_cant INT NOT NULL,
  pr_pais CHARACTER VARYING (20) NOT NULL,
  pr_fecha timestamp without time zone NOT NULL,
  PRIMARY KEY (pr_serial));

CREATE TABLE "despacho_producto" (
  des_id serial NOT NULL,
  fk_producto INT NOT NULL,
  des_motivo CHARACTER VARYING (80) NOT NULL,
  PRIMARY KEY (des_id)
  );

CREATE TABLE "historico_estado_producto" (
  id_estado serial NOT NULL,
  fk_producto INT NOT NULL,
  desc_es CHARACTER VARYING (80) NOT NULL,
  PRIMARY KEY (id_estado)
  );



     ALTER TABLE "despacho_producto" ADD CONSTRAINT fk_producto
            FOREIGN KEY (fk_producto)
            REFERENCES "producto" (pr_serial);

      ALTER TABLE "historico_estado_producto" ADD CONSTRAINT fk_producto
             FOREIGN KEY (fk_producto)
             REFERENCES "producto" (pr_serial);


